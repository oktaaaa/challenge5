const express = require('express')
const app = express()
const PORT = 8080;

//logger
const morgan = require('morgan')
app.use(morgan('dev'))

//public
app.use(express.static('public'));

//view engine
app.set('view engine', 'ejs')

//page route
const pageRoute = require('./route/gameRoute')
app.use(pageRoute)

app.use(express.json());

//user actions
const userActions = require('./controller/user-actions.js')
app.use('/user-action', userActions)

//port
app.listen(PORT, () => {
    console.log(`Server is listening on ${PORT}`);
});