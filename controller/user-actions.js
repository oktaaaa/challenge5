const express = require('express')
const action = express.Router()

const fs = require('fs')
let users = require('../models/users-list.json')

action.post('/signup', (req, res) =>{
    const {name, username, password, memo} = req.body
    const user = {name, username, password}

    if (!username || !password || !name){
        res.sendStatus(400)
        return
    }

    const existingUser = users.find((user) => user.username === username)

    if(existingUser){
        res.status(409).json({
          memo: 'username already exists'
        })
    }

    users.push(user)
    fs.writeFileSync('./models/users-list.json', JSON.stringify(users))
    res.status(201).json({
      name: user.name,
      username: user.username,
      password:user.password
    })
})


//get the users
action.get('/login', (req, res) => {
    const {username, password, memo} = req.body
  
    if (!username || !password){
      res.status(400).json({
        memo: 'please fill out'
      })
    }
  
    const existingUser = users.find(user => user.username === username)
  
    if (!existingUser || existingUser.password !== password){
      res.sendStatus(401)
      return
    }
  
    res.status(200).json({
      name: existingUser.name,
      username: existingUser.username
    })
})

action.delete('/logout', (req, res, next) =>{
    const {name, username, password} = req.body
    const user = {name, username, password}

    const existingUser = users.find((user) => user.username === username)

    if(existingUser){
        users.pop(user)
        return
    }
    fs.writeFileSync('./models/users-list.json', JSON.stringify(users))
    res.sendStatus(201).json()
})
module.exports = action