const express = require('express')
const router = express.Router()
let isLogin = true

router.get('/', (req, res) =>{
    res.render('index')
})

router.get('/play-game', (req, res) =>{
    if(isLogin){
        res.render('rsp_game')
        return
    }
    
})

router.use((req, res) => {
    res.status(404).render('404');
});

module.exports = router
